var config = {
    style: "mapbox://styles/enocks4seth/cjexlfn160l7t2sqvz3gdbqwd",
    accessToken:
        "pk.eyJ1IjoiZW5vY2tzNHNldGgiLCJhIjoiY2tvZHltZzlwMDcydzJ3cWdvaGYwdTdtcyJ9.06OiMGTiAnQnpjpec8JmnQ",
    showMarkers: true,
    markerColor: "#3FB1CE",
    theme: "light",
    use3dTerrain: false,
    title: "SDG 3.2",
    subtitle:
        "By 2030, end preventable deaths of newborns and children under 5 years of age, with all countries aiming to reduce neonatal mortality to at least as low as 12 per 1000 live births and under-5 mortality to at least as low as 25 per 1000 live births. This storymap show top 10 countries with higher Countries by maternal mortality ratio. 2017 (maternal deaths per 100,000 live births)",
    byline: "",
    footer:
        'Data Source: <a href="https://en.wikipedia.org/wiki/List_of_countries_by_maternal_mortality_ratio">Wikipedia: List of countries by maternal mortality ratio</a>, Map Template Based on: <a href="https://github.com/mapbox/storytelling">Storytelling</a> by Mapbox',
    chapters: [
        {
            id: "sudan",
            alignment: "left",
            hidden: false,
            title: "Sudan",
            image:
                "https://s.abcnews.com/images/Health/WireAP_43a6511f44244070adf9f69c16205e55_16x9_992.jpg",
            description:
                'Sudan has the highest Marternal mortality ratio of <strong>1,150</strong> per 100,000 live births. <br> Image Source: <a href="https://abcnews.go.com/Health/wireStory/south-sudan-midwives-bring-deaths-odds-62375397">ABC News</a>',
            location: {
                center: [30.26585, 15.59412],
                zoom: 5,
                pitch: 0,
                bearing: 0,
            },
            mapAnimation: "flyTo",
            rotateAnimation: false,
            callback: "",
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1,
                //     duration: 5000
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ],
        },
        {
            id: "chad",
            alignment: "right",
            hidden: false,
            title: "Chad",
            image: "",
            description:
                "Chad has the second highest Marternal mortality ratio of <strong>1,140 </strong> per 100,000 live births",
            location: {
                center: [18.90249, 15.45801],
                zoom: 5,
                pitch: 0,
                bearing: 0,
            },
            mapAnimation: "flyTo",
            rotateAnimation: false,
            callback: "",
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1,
                //     duration: 5000
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ],
        },
        {
            id: "sierra",
            alignment: "left",
            hidden: false,
            title: "Sierra Leone",
            image: "",
            description:
                "Sierra has a maternal mortality ratio of <strong>1,150</strong> per 100,000 live births",
            location: {
                center: [-11.87213, 8.61691],
                zoom: 5,
                pitch: 0,
                bearing: 0,
            },
            mapAnimation: "flyTo",
            rotateAnimation: false,
            callback: "",
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1,
                //     duration: 5000
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ],
        },
        {
            id: "nigeria",
            alignment: "right",
            hidden: false,
            title: "Nigeria",
            image: "",
            description:
                "Nigeria has marternal mortality ratio of <strong>917</strong> per 100,000 live births",
            location: {
                center: [8.20752, 10.28721],
                zoom: 5.13,
                pitch: 0,
                bearing: 0,
            },
            mapAnimation: "flyTo",
            rotateAnimation: false,
            callback: "",
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1,
                //     duration: 5000
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ],
        },
        {
            id: "somalia",
            alignment: "left",
            hidden: false,
            title: "Somalia",
            image: "",
            description:
                "Somalia has marternal mortality ratio of <strong>829</strong> per 100,000 live births",
            location: {
                center: [48.40758, 7.47625],
                zoom: 3.77,
                pitch: 0,
                bearing: 0,
            },
            mapAnimation: "flyTo",
            rotateAnimation: false,
            callback: "",
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1,
                //     duration: 5000
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ],
        },
        {
            id: "car",
            alignment: "right",
            hidden: false,
            title: "Central African Republic",
            image: "",
            description:
                "Central African Republic has marternal mortality ratio of <strong>829</strong> per 100,000 live births",
            location: {
                center: [20.97534, 8.33169],
                zoom: 3.92,
                pitch: 0.0,
                bearing: 0.0,
            },
            mapAnimation: "flyTo",
            rotateAnimation: false,
            callback: "",
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1,
                //     duration: 5000
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ],
        },
        {
            id: "mauritania",
            alignment: "left",
            hidden: false,
            title: "Mauritania",
            image: "",
            description:
                "Mauritania has marternal mortality ratio of <strong>766</strong> per 100,000 live births",
            location: {
                center: [-10.54167, 20.38504],
                zoom: 5.78,
                pitch: 0.0,
                bearing: 0.0,
            },
            mapAnimation: "flyTo",
            rotateAnimation: false,
            callback: "",
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1,
                //     duration: 5000
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ],
        },
        {
            id: "guinea-bissau",
            alignment: "right",
            hidden: false,
            title: "Guinea-Bissau",
            image: "",
            description:
                "Mauritania has marternal mortality ratio of <strong>667</strong> per 100,000 live births",
            location: {
                center: [-14.97353, 12.27329],
                zoom: 7.69,
                pitch: 0.00,
                bearing: 0.00
            },
            mapAnimation: "flyTo",
            rotateAnimation: false,
            callback: "",
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1,
                //     duration: 5000
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ],
        },
        {
            id: "liberia",
            alignment: "left",
            hidden: false,
            title: "Liberia",
            image: "",
            description:
                "Liberia has marternal mortality ratio of <strong>661</strong> per 100,000 live births",
            location: {
                center: [-9.75214, 6.77157],
                zoom: 5.91,
                pitch: 0.00,
                bearing: 0.00
            },
            mapAnimation: "flyTo",
            rotateAnimation: false,
            callback: "",
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1,
                //     duration: 5000
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ],
        },
        {
            id: "Afghanistan",
            alignment: "right",
            hidden: false,
            title: "Afghanistan",
            image: "",
            description:
                "Afghanistan has marternal mortality ratio of <strong>638</strong> per 100,000 live births",
            location: {
                center: [66.04461, 34.94418],
                zoom: 4.90,
                pitch: 0.00,
                bearing: 0.00
            },
            mapAnimation: "flyTo",
            rotateAnimation: false,
            callback: "",
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1,
                //     duration: 5000
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ],
        },
    ],
};
